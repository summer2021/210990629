# OneBot RPC - 事件

OneBot 标准中包含了对传输数据格式的规定和要求，下面是对事件的定义和传输的说明。

## 格式

- 每个事件的本体都**必须**使用 JSON 格式进行传递
- 事件**必须**是由 OneBot 实现或 LibOneBot 库发出，由 OneBot 机器人逻辑端接收的数据
- 事件**不得**包含消息事件、通知事件、请求事件和元事件之外的主类型事件

## 字段

OneBot 事件分为核心事件集（Core Event Set）和扩展事件集（Extended Events）。

无论核心事件集还是扩展事件集，都**必须**包含以下字段：

字段名 | 变量类型 | 说明
--- | --- | ---
`time` | int64 | 事件发生的时间戳
`self_id` | string | 一般用于表明 OneBot 实现自身的身份 ID，如 TG 机器人可指定为机器人自身的 ID
`type` | string | 事件类型名称，**必须**是 `message`、`notice`、`request`、`meta_event` 的其中一种
`detail_type` | string | 事件的子类型，一般用作区分对于消息或通知、请求类事件的具体细分类型，核心事件集规定群组以及私聊相关的子事件**必须**使用 `group` 和 `private`，对于其他需要自行扩展的，**必须**使用平台专有前缀，如 `qq_`
`sub_type` | string | 一般用作事件具体类型的表明，可为空，但不存在时**必须**使用空字符串，**不可**缺少此参数（例如：QQ 群内的禁言通知，可能区分为解禁或禁言两种类型）
`id` | int64 | 事件的唯一 ID，不同事件不可以使用同一个 ID
`platform` | string | 平台名称

对于类型为 `message` 的事件，在 `detail_type` 为 `group` 时**必须**包含以下字段：

字段名 | 变量类型 | 说明
--- | --- | ---
`group_id` | string | 群自身 ID

对于类型为 `request` 的事件，**必须**包含以下字段：

字段名 | 变量类型 | 说明
--- | --- | ---
`flag` | string | 表明请求事件本身的标记号，用于响应请求使用

对于扩展事件，如果需要带有额外的参数，**需要**将额外的参数使用前缀 `xxx_`（xxx 为平台对应的 `platform`）。

下面是一个典型的符合 OneBot RPC 规范的事件数据格式：

```json
{
    "self_id": "123234",
    "messsage": [
        {
            "type": "text",
            "data": {
                "text": "OneBot is not a bot"
            }
        }
    ],
    "message_id": "6283",
    "id": 80203,
    "type": "message",
    "detail_type": "private",
    "user_id": "123456788",
    "sub_type": "",
    "qq_nickname": "海阔天空",
    "platform": "qq"
}
```

在不同实现中实现扩展字段时应注意：

- 核心事件的扩展字段：加 `platform` 前缀，如 `qq_nickname`
- 扩展事件的 `detail_type`：加 `platform` 前缀，同时其它字段任意扩展，如 `qq_discuss`（纪念不复存在的讨论组）
