OneBot 标准涉及到客户端和服务端通信，在采用 OneBot 标准的聊天机器人软件上**需要**实现以下通信方式的至少一种， 目前 OneBot 标准包括四种通信方式：

- **HTTP**：OneBot 实现作为 HTTP 服务端，提供接口调用服务
- **HTTP Webhook**：OneBot 实现作为 HTTP 客户端，向用户配置的地址推送事件
- **正向 WebSocket**：OneBot 实现作为服务端监听端口，接受客户端的接入，提供 API 调用和事件推送服务
- **反向 WebSocket**：OneBot 实现作为客户端，向用户配置的地址发起连接，提供 API 调用和事件推送服务

在非特殊说明的地方，所有通信方式传输的字符串类型数据都**必须**使用 UTF-8 编码。

在 OneBot 实现的开发上，如果使用 LibOneBot 库，则默认会兼容所有的通信方式，开发者**推荐**专注于实现对应聊天平台的协议转换即可。

在 OneBot 机器人的开发上，开发者**推荐**只选择一种通信方式进行对接，降低复杂性。