聊天机器人可以用来做很多有意思的事情，这里列出一些基于 OneBot 标准的应用案例，欢迎补充。

| 项目地址                                                     | 简介                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [milkice233/efb-qq-slave](https://github.com/milkice233/efb-qq-slave) | 基于 ehForwarderBot 框架的 QQ 从端                           |
| [projectriri/bot-gateway](https://projectriri.github.io/bot-gateway/) | 提供跨聊天平台的通用机器人 API 的机器人消息网关              |
| [jqqqqqqqqqq/UnifiedMessageRelay](https://github.com/jqqqqqqqqqq/UnifiedMessageRelay) | QQ <-> Telegram Bot Framework & Forwarder                    |
| [Mother-Ship/cabbageWeb](https://github.com/Mother-Ship/cabbageWeb) | 基于 Java Web 的 osu! 游戏数据查询机器人                     |
| [bdbai/Kobirt](https://github.com/bdbai/Kobirt)              | Ingress 游戏辅助机器人                                       |
| [JRT-FOREVER/hub2coolq](https://github.com/JRT-FOREVER/hub2coolq) | GitHub webhook 消息转发至 QQ 群                              |
| [Kurarion/Bangumi-for-QQ](https://github.com/Kurarion/Bangumi-for-QQ) | 用于在 QQ 平台便捷地使用 Bangumi 部分功能（查寻条目、更新条目进度等） |
| [rikakomoe/cqhttp-twitter-bot](https://github.com/rikakomoe/cqhttp-twitter-bot) | 自动订阅 Twitter 发送到 QQ                                   |
| [XiaoLin0815/QQ2TG](https://github.com/XiaoLin0815/QQ2TG)    | 帮助 QQ 与 Telegram 互联的小程序                             |
| [spacemeowx2/splatoon2-qqbot](https://github.com/spacemeowx2/splatoon2-qqbot) | 宇宙第一的 Splatoon2 的地图机器人                            |
| [OYMiss/forward-bot](https://github.com/OYMiss/forward-bot)  | 用 Telegram 和 QQ 好友聊天的转发机器人                       |
| [mrthanlon/SICNUBOT](https://github.com/mrthanlon/SICNUBOT)  | 专为四川师范大学设计用于审核发布消息用的 QQ 机器人           |
| [billjyc/pocket48](https://github.com/billjyc/pocket48/tree/coolq) | 监控成员口袋 48 聚聚房间、微博和摩点项目                     |
| [chinshin/CQBot_hzx](https://github.com/chinshin/CQBot_hzx)  | 摩点 & 微博 & 口袋 48 机器人（BEJ48-黄子璇）                 |
| [Ice-Hazymoon/grnd_bot](https://github.com/Ice-Hazymoon/grnd_bot) | 订阅 RSSHub 更新并推送到 QQ 群                               |
| [Ray-Eldath/Avalon](https://github.com/Ray-Eldath/Avalon)    | 多功能、可扩展的群机器人，支持 QQ 和 Discord                 |
| [Bluefissure/FFXIVBOT](https://github.com/Bluefissure/FFXIVBOT) | 基于 Django Channels 的最终幻想 14 游戏数据查询机器人        |
| [Milkitic/Daylily](https://github.com/Milkitic/Daylily)      | 基于 ASP.NET Core 的跨平台机器人（含快速开发插件框架）       |
| [cczu-osa/aki](https://github.com/cczu-osa/aki)              | 基于 NoneBot 的多功能 QQ 机器人                              |
| [cleoold/sendo-erika](https://github.com/cleoold/sendo-erika) | 基于 cqhttp 和 NoneBot 的，主要通过私聊摇控的 QQ 机器人      |
| [duan602728596/qqtools](https://github.com/duan602728596/qqtools) | 基于 Nwjs 的 QQ 群工具（摩点、口袋 48、微博提醒、入群欢迎、定时喊话、自定义命令和回复信息等） |
| [Tsuk1ko/CQ-picfinder-robot](https://github.com/Tsuk1ko/CQ-picfinder-robot) | 基于 Saucenao 的搜图机器人                                   |
| [kasora/dice](https://github.com/kasora/dice)                | COC7 骰子 QQ 机器人                                          |
| [ac682/arcbot](https://github.com/ac682/arcbot)              | 基于 [ProjHyperai](https://github.com/theGravityLab/ProjHyperai) 的群基础机器人，提供推送转发和一些娱乐功能 |
| [shidenggui/tuishujun-for-qq](https://github.com/shidenggui/tuishujun-for-qq) | 基于推书君的小说查询推荐 QQ 机器人                           |
| [JuerGenie/cn.juerwhang.jgbot](https://github.com/JuerGenie/cn.juerwhang.jgbot) | 基于 [JuerGenie/juerobot](https://github.com/JuerGenie/juerobot) 的娱乐用 QQ 机器人 |
| [drsanwujiang/DiceRobot](https://github.com/drsanwujiang/DiceRobot) | 一个基于 coolq-http-api 插件的 TRPG 骰子机器人               |
| [UltraSoundX/SDFMU-Library](https://github.com/UltraSoundX/SDFMU-Library) | 山东第一医科大图书馆预约机器人                               |
| [Quan666/ELF_RSS](https://github.com/Quan666/ELF_RSS)        | 基于 NoneBot 的，交互式 RSS 订阅、转发机器人                 |
| [lz1998/Spring-CQ-web](https://github.com/lz1998/Spring-CQ-web) | 基于 SpringCQ 的机器人 web 控制台                            |
| [suisei-cn/stargazer-qq](https://github.com/suisei-cn/stargazer-qq) | 一个灵活的 vtuber 发推/直播动态监控机器人                    |
| [Ninzore/Wecab](https://github.com/Ninzore/Wecab)            | 网络内容聚合机器人，支持微博、B站、Twitter 等                |
| [mgsky1/FG](https://github.com/mgsky1/FG)                    | 基于 NoneBot 的 QQ 群机器人，特色功能是利用机器学习算法提取每日的聊天热词，并使用词云+文本的方式进行展示 |
| [Yiwen-Chan/ZeroBot-Plugin](https://github.com/Yiwen-Chan/ZeroBot-Plugin) | 基于 ZeroBot 开发的群管、点歌、搜图等功能                    |
| [yuudi/gypsum](https://github.com/yuudi/gypsum)              | 简单易用的网页控制台，匹配消息进行回复，也可以使用 jinja 模板与 lua 脚本实现高级功能 |